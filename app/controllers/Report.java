package controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import models.Candidate;
import models.Donation;
import models.User;
import play.mvc.*;
import play.Logger;

public class Report extends Controller
	{
	
	/**
	 *  render Report page off all donations: donors( name, age, state), amountDonated, methodDonated, candidates, 
	 *  
	 *  
	 */
	public static void index()
	{
		User user = Accounts.getCurrentUser();
		List<User> users = User.findAll();
		List<Donation> donations = Donation.findAll();
		List<Candidate> candidates = Candidate.findAll();
		List<String> states = Arrays.asList("Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", 
				"Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana ",
				"Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana Nebraska", "Nevada",
				"New Hampshire", "New Jersey", "New Mexico", "New York ", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon ",
				"Pennsylvania Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia",
				"Washington", "West Virginia", "Wisconsin", "Wyoming");
		render(user, users, donations, candidates, states);
	}
	
	/**
	 * Method to filter donations by candidate.
	 * @param candidate
	 */

	public static void filterCandidates(String candidate) {
		String userId = session.get("logged_in_userid");
		if (userId != null) {
			User user = User.findById(Long.parseLong(userId));
			List<Donation> donations = new ArrayList();
			List<Donation> allDonations = Donation.findAll();
			Candidate foundCandidate = Candidate.findById(Long
					.parseLong(candidate));
			for (Donation donation : allDonations) {
				// donation.from.candidate for story 7
				if (donation.to == foundCandidate) {
					donations.add(donation);
				}
			}
			renderTemplate("Report/index.html", user, donations, foundCandidate);
		} else {
			Logger.info("User not logged in");
			Accounts.login();
		}

	}
	
	/**
	 * Method to filter donations by user(donor).
	 * @param donor
	 */

	public static void filterDonors(String donor) {
		String userId = session.get("logged_in_userid");
		if (userId != null) {
			User user = User.findById(Long.parseLong(userId));
			User foundUser = User.findById(Long.parseLong(donor));
			List<Donation> donations = new ArrayList();
			List<Donation> allDonations = Donation.findAll();

			for (Donation donation : allDonations) {
				if (donation.from == foundUser) {
					donations.add(donation);
				}
			}
			renderTemplate("Report/index.html", user, donations, foundUser);
		} else {
			Logger.info("User not logged in");
			Accounts.login();
		}
	}
	
	/**
	 * Method to filter donations by states.
	 * @param state1
	 */

	public static void filterStates(String state1) {
		String userId = session.get("logged_in_userid");
		if (userId != null) {
			User user = User.findById(Long.parseLong(userId));
			List<Donation> allDonations = Donation.findAll();
			List<Donation> donations = new ArrayList();
			List<String> states = Arrays.asList("Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", 
					"Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana ",
					"Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana Nebraska", "Nevada",
					"New Hampshire", "New Jersey", "New Mexico", "New York ", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon ",
					"Pennsylvania Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia",
					"Washington", "West Virginia", "Wisconsin", "Wyoming");
			
			for (int i = 0; i < allDonations.size(); i++) {
				if (allDonations.get(i).from.state.equals(state1)) {
					donations.add(allDonations.get(i));
				}
			}
			renderTemplate("Report/index.html", user, donations, states);
		} else {
			Logger.info("User not logged in");
			Accounts.login();
		}

	}
}
