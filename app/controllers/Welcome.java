package controllers;

import play.Logger;
import play.mvc.Controller;

/**
 * 
 * render home page
 *
 */
public class Welcome extends Controller {
	public static void index() {
		Logger.info("Landed in Welcome class");
		render();
	}
}
