package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Candidate;
import models.Donation;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class DonationController extends Controller {

	/**
	 * render page for making donations method to get all candidates individual
	 * progresses for a target
	 */

	public static void index() {
		User user = Accounts.getCurrentUser();
		List<Candidate> candidates = Candidate.findAll();
		if (user == null) {
			Logger.info("Donation class : Unable to getCurrentUser");
			Accounts.login();
		} else {
			getPercentTargetAchieved();
			Logger.info("Donation ctrler : user is " + user.email);
			Logger.info("Donation ctrler : percent target achieved");
			render(user, candidates);
		}

	}

	/**
	 * method for donation action
	 * 
	 * @param candidateId
	 * @param amountDonated
	 * @param methodDonated
	 */

	public static void donate(String candidateId, long amountDonated,
			String methodDonated) {
		Logger.info("candidate id" + candidateId + "" + "amount donated"
				+ amountDonated + "" + "method donated" + methodDonated);
		Candidate target = Candidate.findById(Long.parseLong(candidateId));
		User user = Accounts.getCurrentUser();
		if (user == null) {
			Logger.info("Donation class : Unable to getCurrentUser");
			Accounts.login();
		} else {
			addDonation(user, target, amountDonated, methodDonated);
		}
		index();
	}

	/**
	 * method to add new donation
	 * 
	 * @param user
	 * @param candidate
	 * @param amountDonated
	 * @param methodDonated
	 */

	private static void addDonation(User user, Candidate candidate,
			long amountDonated, String methodDonated) {
		Donation bal = new Donation(user, candidate, amountDonated,
				methodDonated);
		bal.save();

	}

	/**
	 * method to get donation's target
	 * 
	 * @param donationTarget
	 */

	private static long getDonationTarget() {

		List<Candidate> candidates = Candidate.findAll();
		long donationTarget = 0;
		for (Candidate candidate : candidates) {
			// long donationTarget = 0;
			for (int i = 0; i < candidates.size(); i++) {
				long newtarget = candidates.get(i).donationTarget;
				donationTarget = donationTarget += newtarget;
				candidate.donationTarget = donationTarget;
				candidate.save();
			}
		}
		return donationTarget;
	}

	private static void getPercentTargetAchieved() {
		List<Candidate> candidates = Candidate.findAll();
		for (Candidate candidate : candidates) {

			long progress = 0;
			for (int i = 0; i < candidate.donations.size(); i++) {
				long a = candidate.donations.get(i).received;
				progress = progress += a;

			}

			progress = progress * 100 / getDonationTarget();
			candidate.progress = progress;
			candidate.save();
		}

	}

}
