package controllers;

import java.util.List;

import models.Admin;
import models.Candidate;
import models.User;
import play.Logger;
import play.mvc.Controller;


public class Accounts extends Controller {
	public static void index() {
		render();
	}

	/**
	 * render page to register as a new user
	 */
	public static void signup() {
		List<Candidate> candidates = Candidate.findAll();
		render(candidates);
	}
	
	/**
	 *render page to edit details for a user
	 */

	public static void edit() {
		User user = Accounts.getCurrentUser();
		render(user);
	}
	
	/**
	 * method to change user's details
	 * @param firstName
	 * @param lastName
	 * @param age
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 * @param zipCode
	 */

	public static void editprofile(String firstName, String lastName,
			String age, String address1, String address2, String city,
			String state, String zipCode)
	{
		User user = Accounts.getCurrentUser();
		Logger.info(firstName + " " + lastName + " " + age + " " + address1
				+ " " + address2 + " " + city + " " + state + " " + zipCode);

		if (!firstName.isEmpty())
			user.lastName = firstName;

		if (!lastName.isEmpty())
			user.lastName = lastName;

		if (age != null)
			user.age = age;

		if (!address1.isEmpty())
			user.address1 = address1;

		if (!address2.isEmpty())
			user.address2 = address2;

		if (!city.isEmpty())
			user.city = city;

		if (!state.isEmpty())
			user.state = state;

		if (!zipCode.isEmpty())
			user.zipCode = zipCode;

		user.save();

		login();
	}
	
	/**
	 * method to register new user
	 * @param usaCitizen
	 * @param firstName
	 * @param lastName
	 * @param age
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 * @param zipCode
	 * @param email
	 * @param password
	 * @param candidate
	 */

	public static void register(boolean usaCitizen, String firstName,
			String lastName, String age, String address1, String address2,
			String city, String state, String zipCode, String email,
			String password, String candidate) {
		Logger.info(usaCitizen + " " + firstName + " " + lastName + " " + age
				+ " " + address1 + " " + address2 + " " + city + " " + state
				+ " " + zipCode + " " + email + " " + password);
		// code for story 6
		//Candidate foundCandidate = Candidate.findById(Long.parseLong(candidate));

		User user = new User(usaCitizen, firstName, lastName, age, address1,
				address2, city, state, zipCode, email, password);
		//user.candidate = foundCandidate;
		user.save();
		login();
	}
	
	/**
	 * render login page
	 */

	public static void login() {
		render();
	}
	
	/**
	 *  logout action
	 */
	
	public static void logout() {
		session.clear();
		Welcome.index();
	}
	
	
	/**
	 * method to authenticate user 
	 * @param email
	 * @param password
	 */
	
	public static void authenticate(String email, String password) {
		Logger.info("Attempting to authenticate with " + email + ":" + password);

		User user = User.findByEmail(email);
		if ((user != null) && (user.checkPassword(password) == true)) {
			Logger.info("Successfull authentication of  " + user.firstName
					+ " " + user.lastName);
			session.put("logged_in_userid", user.id);
			DonationController.index();
		} else {
			Logger.info("Authentication failed");
			Accounts.login();
		}
	}

	
	/**
	 * method to get current user
	 * @return
	 */
	public static User getCurrentUser() {
		String userId = session.get("logged_in_userid");
		if (userId == null) {
			return null;
		}
		User logged_in_user = User.findById(Long.parseLong(userId));
		Logger.info("In Accounts controller: Logged in user is "
				+ logged_in_user.firstName);
		return logged_in_user;
	}
}