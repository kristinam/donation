package controllers;

import java.util.List;

import models.Admin;
import models.Candidate;
import models.Office;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class OfficeController extends Controller {
	
	/**
	 * render page to create new office
	 */
	public static void office() {
		render();
	}

	/**
	 * method to create new office
	 * @param officeTitle
	 * @param officeDescription
	 * @param email
	 */
	public static void registerOffice(String officeTitle,
			String officeDescription) {

		Logger.info(officeTitle + " " + officeDescription);

		Office office = new Office(officeTitle, officeDescription);
		office.save();
		Administrator.index();
	}

}
