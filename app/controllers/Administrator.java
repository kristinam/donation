package controllers;

import java.util.List;

import models.Admin;
import models.Candidate;
import models.Office;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class Administrator extends Controller {
	
	/**
	 * render page for administrator to log in 
	 * name: admin
	 * password: secret
	 */
	public static void admin() {
		render();
	}
	
	/**
	 * method for administrator to login
	 * @param email
	 * @param password
	 */

	public static void adminlogin(String email, String password) {

		Logger.info("Attempting to authenticate with " + email + ":" + password);

		Admin admin = Admin.findByEmail(email);
		if ((admin != null) && (admin.checkPassword(password) == true)) {
			Logger.info("Successfull authentication of  " + email);
			session.put("logged_in_adminid", admin.id);
			Administrator.index();
		} else {
			Logger.info("Authentication failed");
			Welcome.index();
		}
	}

	
	/**
	 * render report list of  users-donors and candidates with their details;
	 */
	public static void index() {

		List<User> users = User.findAll();
		List<Candidate> candidates = Candidate.findAll();
		List<Office> offices = Office.findAll();
		
		render(users, candidates, offices);

	}
	
	/**
	 * logout action for administrator
	 */

	public static void logout() {
		session.clear();
		Welcome.index();
	}
	
	/**
	 * render page to register candidates by administrator
	 */

	public static void register() {
		List<Office> offices = Office.findAll();
		List<Candidate> candidates = Candidate.findAll();
		render(offices, candidates);
	}
	
	
	/**
	 * method to register candidates by administrator
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param progress
	 * @param office
	 */
	public static void registerCandidate(String firstName, String lastName, String email, int progress, String office, int target ) {
		Logger.info( firstName + " " + lastName + " " + email  );
		
		Office foundOffice = Office.findById(Long.parseLong(office));
		Candidate candidate = new Candidate(firstName, lastName, email, progress, target);
		candidate.office = foundOffice;
		candidate.save();
		index();

	}

}
