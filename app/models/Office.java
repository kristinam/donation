package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

@Entity
/**
 * 
 * Office class for offices that candidates may run
 *
 */
public class Office extends Model {
	public String officeTitle;
	public String officeDescription;

	@OneToMany(mappedBy = "office", cascade = CascadeType.ALL)
	public List<Candidate> candidates;

	/**
	 * Office parameters
	 * 
	 * @param officeTitle
	 * @param officeDescription
	 */
	public Office(String officeTitle, String officeDescription) {
		this.officeTitle = officeTitle;
		this.officeDescription = officeDescription;
		

	}
}
