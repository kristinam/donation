package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

@Entity

/**
 * 
 * Donation class for donations that will be done by users to candidates.
 *
 */
public class Donation extends Model {
	public long received;
	public String methodDonated;

	@ManyToOne
	public User from;

	@ManyToOne
	public Candidate to;
	
	/**
	 * parameters of donations
	 * @param source
	 * @param target
	 * @param received
	 * @param methodDonated
	 */

	public Donation(User source, Candidate target, long received,
			String methodDonated) {
		from = source;
		to = target;
		this.received = received;
		this.methodDonated = methodDonated;
	}

}
