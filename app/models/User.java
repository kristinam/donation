package models;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

@Entity

/**
 * 
 * User class for users-donors witch be doing donations to candidates.
 *
 */
public class User extends Model {
	public boolean usaCitizen;
	public String firstName;
	public String lastName;
	public String age;
	public String address1;
	public String address2;
	public String city;
	public String state;
	public String zipCode;
	public String email;
	public String password;

	@OneToMany(mappedBy = "from", cascade = CascadeType.ALL)
	List<Donation> donations = new ArrayList<Donation>();
	//public Candidate candidate;

	
	/**
	 * Users parameters:
	 * @param usaCitizen
	 * @param firstName
	 * @param lastName
	 * @param age
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 * @param zipCode
	 * @param email
	 * @param password
	 */

	public User(boolean usaCitizen, String firstName, String lastName,
			String age, String address1, String address2, String city,
			String state, String zipCode, String email, String password

	) {
		this.usaCitizen = usaCitizen;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.email = email;
		this.password = password;

	}

	
	/**
	 * method to find user by his email
	 * @param email
	 * @return
	 */
	public static User findByEmail(String email) {
		return find("email", email).first();
	}

	
	/**
	 * method to check are passwords the same
	 * @param password
	 * @return
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}

}