package models;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

@Entity

/**
 * 
 * Admin class for administrator who can create new candidates, new offices 
 * and see all lists of candidates, users;
 *
 */

public class Admin extends Model {

	
	public String adminName;
	public String email;
	public String password;

	
	/**
	 * parameters of administrator
	 * @param adminName
	 * @param email
	 * @param password
	 */
	public Admin(String adminName, String email, String password)
	{
		this.adminName = adminName;
		this.email = email;
		this.password = password;
	}
	
	/**
	 * method to check are passwords equal
	 * @param password
	 * @return
	 */
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}
	
	/**
	 * method to find administrator by email
	 * @param email
	 * @return
	 */
	public static Admin findByEmail(String email) {
		return find("email", email).first();
	}

}