package models;

import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.db.jpa.Model;

@Entity

/**
 * 
 * Candidates class for candidates to whom donations will be done.
 *
 */

public class Candidate extends Model {

	public String firstName;
	public String lastName;
	public String email;
	public long progress;
	public int total;
	public long donationTarget;

	@OneToMany(mappedBy = "to", cascade = CascadeType.ALL)
	 public List<Donation> donations;
	@ManyToOne
	public Office office;
	
	/**
	 * parameters of candidates
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param progress
	 * @param target
	 */
	public Candidate(String firstName, String lastName, String email, int progress, int donationTarget)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.progress = progress;
		this.donations = new ArrayList<Donation>();
		this.donationTarget = donationTarget;

	}

	  
	
}