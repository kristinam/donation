/**
 * 
 */

$('.ui.dropdown').dropdown();

$('.ui.form').form({
	candidate : {
		identifier : 'candidate',
		rules : [ {
			type : 'empty',
			prompt : 'Please select candidate'
		} ]
	},
	donor : {
		identifier : 'donor',
		rules : [ {
			type : 'empty',
			prompt : 'Please select donor'
		} ]
	},
	state : {
		identifier : 'state',
		rules : [ {
			type : 'empty',
			prompt : 'Please select state'
		} ]
	}
	
});	