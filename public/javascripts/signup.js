$('.ui.dropdown').dropdown();

$('.ui.form').form({
	candidate : {
		identifier : 'candidate',
		rules : [ {
			type : 'empty',
			prompt : 'Please select candidate'
		} ]
	},
	email : {
		identifier : 'email',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in email'
		} ]
	},
	password : {
		identifier : 'password',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in password'
		} ]
	},
	firstName : {
		identifier : 'firstName',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your  first name'
		} ]
	},
	lastName : {
		identifier : 'lastName',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your surname'
		} ]
	},
	age : {
		identifier : 'age',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your age'
		} ]
	},
	city : {
		identifier : 'city',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your city'
		} ]
	},
	state : {
		identifier : 'state',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your state'
		} ]
	},
	zipCode : {
		identifier : 'zipCode',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your zip code'
		} ]
	},
	address1 : {
		identifier : 'address1',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your address1'
		} ]
	},
	address2 : {
		identifier : 'address2',
		rules : [ {
			type : 'empty',
			prompt : 'Please type in your address2'
		} ]
	}
	
});
